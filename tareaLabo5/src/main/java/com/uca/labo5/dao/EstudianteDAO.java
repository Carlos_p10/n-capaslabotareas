package com.uca.labo5.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.uca.labo5.domain.Estudiante;

public interface EstudianteDAO {
	
	public void insertar(Estudiante e) throws DataAccessException;
	
	public List<Estudiante> findAll() throws DataAccessException;
	
}
