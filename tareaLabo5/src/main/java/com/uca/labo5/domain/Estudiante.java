package com.uca.labo5.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(schema = "public",name = "estudiante")
public class Estudiante {
	@Id
	@Column(name = "c_usuario")
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private Integer codigoEstudiante;
	
	@Size(min = 1, max = 50, message = "Debe contener de 1 a 50 caracteres") 
	@Column(name = "nombre")
	private String Nombre;
	
	@Size(min = 1, max = 50, message = "Debe contener de 1 a 50 caracteres")
	@Column(name = "apellido")
	private String Apellido;
	
	@Size(min = 1, max = 10, message = "Debe contener de 1 a 10 caracteres")
	@Column(name = "carne")
	private String Carne;
	
	@Size(min = 1, max = 100, message = "Debe contener de 1 a 100 caracteres")
	@Column(name = "carrera")
	private String Carrera;
	
	public Integer getCodigoEstudiante() {
		return codigoEstudiante;
	}
	public void setCodigoEstudiante(Integer codigoEstudiante) {
		this.codigoEstudiante = codigoEstudiante;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getApellido() {
		return Apellido;
	}
	public void setApellido(String apellido) {
		Apellido = apellido;
	}
	public String getCarne() {
		return Carne;
	}
	public void setCarne(String carne) {
		Carne = carne;
	}
	public String getCarrera() {
		return Carrera;
	}
	public void setCarrera(String carrera) {
		Carrera = carrera;
	}
	
	public Estudiante() {}
}