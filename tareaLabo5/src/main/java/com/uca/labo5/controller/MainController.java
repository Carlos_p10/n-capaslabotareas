package com.uca.labo5.controller;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.uca.labo5.dao.EstudianteDAO;
import com.uca.labo5.domain.Estudiante;

@Controller
public class MainController {
	@Autowired
	private EstudianteDAO studentDao;
	
	@RequestMapping("/inicio")
	public ModelAndView index() {
		ModelAndView mav = new ModelAndView();
		mav.addObject("estudiante", new Estudiante());		
		mav.setViewName("index");
		return mav;
	}

	@PostMapping("/procesar")
	public ModelAndView guardar(@Valid @ModelAttribute Estudiante estudent, BindingResult result){
		ModelAndView mav = new ModelAndView();
		
		if(result.hasErrors()) { 
			mav.setViewName("index");
		}
		else { 
			studentDao.insertar(estudent);
			mav.setViewName("index");
		}
		

		return mav;

	}
	
	@RequestMapping("/listado")
	public ModelAndView viewAll(){
		ModelAndView mav = new ModelAndView();
		
		List<Estudiante> estudiantes = null;
		try {
			estudiantes = studentDao.findAll();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		mav.addObject("estudiantes",estudiantes);
		mav.setViewName("main");
		return mav;
	}

}
