package com.uca.labo5.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.uca.labo5.domain.Estudiante;

@Repository
public class EstudianteDAOImpl implements EstudianteDAO {

	@PersistenceContext(unitName="labo5")
	private EntityManager entityManager;
	
	@Override
	@Transactional
	public void insert(Estudiante e) throws DataAccessException {
		entityManager.persist(e);		
	}

	@Override
	public List<Estudiante> findAll() throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuffer sb = new StringBuffer();
		sb.append("select * from public.estudiante");
		Query query = entityManager.createNativeQuery(sb.toString(),Estudiante.class);
		List<Estudiante> resulset= query.getResultList();
		
		return resulset;
	}

	@Override
	public Estudiante findOne(Integer id) throws DataAccessException {
		return entityManager.find(Estudiante.class, id);
	}

	@Override
	@Transactional
	public void delete(Integer codigo) throws DataAccessException {
		Estudiante estudiante = entityManager.find(Estudiante.class, codigo);
		entityManager.remove(estudiante);		
	}
	
}
