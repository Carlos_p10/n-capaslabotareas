package com.uca.labo5.dao;

import java.util.List;
import org.springframework.dao.DataAccessException;
import com.uca.labo5.domain.Estudiante;

public interface EstudianteDAO {
	
	public List<Estudiante> findAll() throws DataAccessException;
	
	public Estudiante findOne(Integer id) throws DataAccessException;
	
	public void insert(Estudiante e) throws DataAccessException;
	
	public void delete(Integer codigo) throws DataAccessException;
	
}
