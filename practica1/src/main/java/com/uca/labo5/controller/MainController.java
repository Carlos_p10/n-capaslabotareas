package com.uca.labo5.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.uca.labo5.dao.ContrubyenteDAO;
import com.uca.labo5.dao.ImportanciaDAO;
import com.uca.labo5.domain.Contribuyente;
import com.uca.labo5.domain.Importancia;


@Controller
public class MainController {

	@Autowired
	private ImportanciaDAO impoDao;
	
	@Autowired
	private ContrubyenteDAO contriDao;
	
	@RequestMapping("/inicio")
	public ModelAndView index() {
		ModelAndView mav = new ModelAndView();
		Contribuyente contri = new Contribuyente();
		
		List<Importancia> impor = null;
		try {
			impor = impoDao.findAll();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		mav.addObject("contribuyente",contri);
		mav.addObject("importancias",impor);
		mav.setViewName("index");
		return mav;
	}

	@PostMapping("/procesar")
	public ModelAndView guardar(@Valid @ModelAttribute Contribuyente contri, BindingResult result){
		ModelAndView mav = new ModelAndView();
		
		
		if(result.hasErrors()) { 
			List<Importancia> impor= null;
			try {
				impor = impoDao.findAll();
			} catch (Exception e) {
				e.printStackTrace();
			}
			mav.addObject("importancias",impor);
			mav.setViewName("index");
		}
		else {
			SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date(System.currentTimeMillis());
			contri.setDate(date);
			contriDao.insertar(contri);
			mav.setViewName("guardado");
		}		
		return mav;

	}
	
	@RequestMapping("/listado")
	public ModelAndView viewAll(){
		ModelAndView mav = new ModelAndView();
		
		List<Contribuyente> contri = null;
		try {
			contri = contriDao.findAll();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		mav.addObject("contribuyentes",contri);
		mav.setViewName("main");
		return mav;
	}

}
