package com.uca.capas.tarea3.domain;

import javax.validation.constraints.AssertTrue;

public class Robot {
	@AssertTrue(message = "UPS por ser robot no puedes agregar el producto, hacer click en regresar.")
	private Boolean soyRobot;	

	public Boolean getSoyRobot() {
		return soyRobot;
	}

	public void setSoyRobot(Boolean soyRobot) {
		this.soyRobot = soyRobot;
	}
	
	

}
