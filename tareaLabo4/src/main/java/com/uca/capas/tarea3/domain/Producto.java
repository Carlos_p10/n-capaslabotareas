package com.uca.capas.tarea3.domain;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Producto {
	@Size(message = "No mas de 15 caracteres.", max = 15)
	@NotEmpty(message = "No puede estar vacio")
	String nombre;
	@Size(message = "No mas de 30 caracteres.", max = 30)
	@NotEmpty(message = "No puede estar vacio")	
	String descripcion;
	@Min(value = 0, message = "El precio no debe ser negativo")
	@NotNull(message = "No puede estar vacio")	
	Double precio;
	@Pattern(regexp = "^(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4}$", message = "Debe ser de tipo dd/MM/yyyy")
	@NotEmpty(message = "No puede estar vacio")	
	String vencimiento;
	
	
	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Double getPrecio() {
		return precio;
	}


	public void setPrecio(Double precio) {
		this.precio = precio;
	}


	public String getVencimiento() {
		return vencimiento;
	}


	public void setVencimiento(String vencimiento) {
		this.vencimiento = vencimiento;
	}

	
	

	public Producto(String nombre, String descripcion, Double precio, String vencimiento) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.precio = precio;
		this.vencimiento = vencimiento;
	}


	public Producto() {	
	}
	
	
	
	
}
