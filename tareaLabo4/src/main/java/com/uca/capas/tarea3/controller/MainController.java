package com.uca.capas.tarea3.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.uca.capas.tarea3.domain.Producto;
import com.uca.capas.tarea3.domain.Robot;

@Controller
public class MainController {
	
	@RequestMapping("/producto")
	public ModelAndView index() {
		ModelAndView mav = new ModelAndView();
		mav.addObject("producto", new Producto());
		mav.setViewName("tarea4/index");
		return mav;
	}

	@RequestMapping("/formProducto")
	public ModelAndView procesar2(@Valid @ModelAttribute Producto pro, BindingResult result) {
		ModelAndView mav = new ModelAndView();
		
		if(result.hasErrors()) { 
			mav.setViewName("tarea4/index");
		}
		else { 			
			mav.addObject("robot", new Robot());
			mav.addObject("nombre", pro.getNombre());
			mav.setViewName("tarea4/robot");
		}
		return mav;
	}
	
	@RequestMapping("/seguridad")
	public ModelAndView seguridad(@Valid @ModelAttribute Robot robot, BindingResult result) {
		ModelAndView mav = new ModelAndView();
		
		if(result.hasErrors()) { 
			mav.setViewName("tarea4/robot");
		}
		else { 
			mav.setViewName("tarea4/agregado");
		}
		return mav;
	}
// kill 8080 port linux: sudo netstat -lpn |grep :8080

}
