package com.uca.labo5.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.uca.labo5.dao.EstudianteDAO;
import com.uca.labo5.domain.Computadora;
import com.uca.labo5.domain.Estudiante;

@Service
public class EstudianteServiceImpl implements EstudianteService {
	@Autowired 
	EstudianteDAO estudianteDao;

	@Override
	public List<Estudiante> findAll() throws DataAccessException {		
		return estudianteDao.findAll();
	}

	@Override
	public Estudiante findOne(Integer code) throws DataAccessException {		
		return estudianteDao.findOne(code);
	}

	@Override
	@Transactional
	public void save(Estudiante estudiante) throws DataAccessException {
		estudianteDao.save(estudiante);
		
	}

	@Override
	@Transactional
	public void delete(Integer codigoEstudiante) throws DataAccessException {
		estudianteDao.delete(codigoEstudiante);		
	}

	@Override
	public List<Computadora> findAllComputadora() throws DataAccessException {
		return estudianteDao.findAllComputadora();
	}

}
