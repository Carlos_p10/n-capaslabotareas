package com.uca.capas.domain;

public class Usuario {

	private String user;
	private String password;
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getContra() {
		return password;
	}
	public void setContra(String contra) {
		this.password = contra;
	}
	
	
	
}
